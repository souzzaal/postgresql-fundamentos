-- Cria uma role sem acesso ao banco de dados
CREATE ROLE professores NOCREATEDB NOCREATEROLE INHERIT NOLOGIN NOBYPASSRLS CONNECTION LIMIT 10;

CREATE TABLE alunos (nome varchar);

-- Concede à role professores todos os privilégios sobre a tabela alunos
GRANT ALL ON TABLE alunos TO professores;

--  Cria uma role/usuário que pode apenas fazer login
CREATE ROLE zezinho LOGIN PASSWORD '123';

-- Neste momento acessando a base com o usuario zezinho não é possível fazer SELECT (nehuma operação, na verdade) em alunos
-- Para fazer login via terminal:  psql -U <user_name> <database_name>

-- Exclui o usuário zezinho
DROP ROLE zezinho;

-- Recria o usuário zezinho pertencendo à role professores
CREATE ROLE zezinho INHERIT LOGIN PASSWORD '123' IN ROLE professores;
-- Agora acessando a base com o usuário zezinho é possível fazer SELECT em alunos

-- Desassocia zezinho de professores
REVOKE professores from zezinho;
-- Agora o usuário zezinho não consegue fazer SELECT em alunos
