CREATE OR REPLACE FUNCTION fc_somar(INTEGER, INTEGER)
RETURNS INTEGER
SECURITY DEFINER
-- RETURNS NULL ON NULL INPUT
CALLED ON NULL INPUT
LANGUAGE SQL
AS $$
    SELECT COALESCE($1, 0) + COALESCE($2, 0); -- COALESCE retorna o primeiro não nulo dos argumentos
$$;

SELECT fc_somar(1,3);
SELECT fc_somar(1, NULL);


--------------------- 

-- PLPGSQL
CREATE OR REPLACE FUNCTION bancos_add (p_numero INTEGER, p_nome VARCHAR, p_ativo BOOLEAN)
RETURNS BOOLEAN
LANGUAGE PLPGSQL
AS $$
    DECLARE variavel_id INTEGER;
    BEGIN
	
		IF p_numero IS NULL OR p_nome IS NULL OR p_ativo IS NULL THEN
			RAISE NOTICE 'Argumentos inválidos'; -- imprime mensagem de erro
			RETURN NULL;
		END IF;
	
        SELECT INTO variavel_id numero FROM banco WHERE numero = p_numero; 
		-- após o SELECT variavel_id guarda o valor do campo numero
        
        IF variavel_id IS NULL THEN
            INSERT INTO banco (numero, nome, ativo) VALUES (p_numero, p_nome, p_ativo);
        ELSE
			RAISE NOTICE 'Código de banco já cadastrado';
            RETURN FALSE;
        END IF;

        SELECT INTO variavel_id numero FROM banco WHERE numero = p_numero;
        IF variavel_id IS NULL THEN
           RAISE NOTICE 'Erro ao cadastrar banco';
		   RETURN FALSE;
        ELSE
            RETURN TRUE; -- PGPSQL necessitar usar "RETURN"
        END IF;
     END; -- END é  o final de um BEGIN dentro de uma função
$$;

SELECT bancos_add(5434, 'Banco teste', false);
select * from banco where numero = 5434;
