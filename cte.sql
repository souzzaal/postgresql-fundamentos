SELECT numero, nome FROM banco;
SELECT banco_numero, numero, nome FROM agencia;


WITH tbl_tmp_banco AS (
	SELECT numero, nome
	FROM banco
)
SELECT numero, nome FROM tbl_tmp_banco;


WITH clientes_e_transacoes AS (
	SELECT 	cliente.nome AS cliente_nome,
			tipo_transacao.nome AS tipo_trasacao_nome,
			cliente_transacoes.valor AS transacao_valor
	FROM cliente_transacoes
	JOIN cliente ON cliente.numero = cliente_transacoes.cliente_numero
	JOIN tipo_transacao ON tipo_transacao.id = cliente_transacoes.tipo_transacao_id	
) 
SELECT * FROM clientes_e_transacoes;

