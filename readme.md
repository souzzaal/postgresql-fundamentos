# PostgreSQL

Projeto para estudo do banco de dados PostgresSQL.

Usando docker para levantar um container servindo o PostgreSQL e outro servindo o PgAdmin4.

Para startar a aplicação:

```shell
docker-compose up -d
```

Para acessar o servidor PostgreSQL:

```shell
docker exec -it postgres_server_1 bash
```

Para fazer login no banco de dados:

```shell
psql -U <user_name> <database_name>
```



Para acessar o PgAdmin:

```http
http://localhost:16543
```

Fazer login com postgres@mail.com e Postgres

Após fazer login no PgAdmin, para fazer a comunicação deste com o servidor PostgreSQL, clique com o botão direito do mouse em "Servers". Em seguida, "Create > Server". Preencha os dados da aba "General", e na aba "Configuration" preencha os seguintes campos com:

- Host/Address name:  server
  
  - "server" é o nome dado ao container do servidor

- Porta:  5432

- Username: postgres

- Password: Postgres



Parar e remover todos os containers, respectivamente:

```shell
docker stop $(docker ps -a -q)
docker rm $(docker ps -a -q)
```





## Referências

[PostgreSQL + pgAdmin 4 + Docker Compose: montando rapidamente um ambiente para uso](https://medium.com/@renato.groffe/postgresql-pgadmin-4-docker-compose-montando-rapidamente-um-ambiente-para-uso-55a2ab230b89)
