# PostgreSQL

O PostgreSQL é um sistema de gerenciamento de banco de dados objeto-relacional (SGBDOR) baseado no POSTGRES Versão 4.2 desenvolvido pelo Departamento de Ciência da Computação da Universidade da Califórnia em Berkeley. Um banco de dados objeto-relacional (ORD), ou sistema de gerenciamento de banco de dados objeto-relacional (ORDBMS ou SGBDOR) é um sistema de gerenciamento de banco de dados relacional que permite aos desenvolvedores integrar ao banco de dados seus próprios tipos de dado e métodos personalizados.

Devido à sua licença liberal, o PostgreSQL pode ser utilizado, modificado e distribuído por qualquer pessoa para qualquer finalidade, seja particular, comercial ou acadêmica, livre de encargos.

## Arquitetura/Hierarquia interna

### Cluster

Coleção de bancos de dados que compartilham as mesmas configurações (arquivos de configuração) do PostgreSQL e do sistema operacional (porta, listen_addresses, etc.). Um cluster pode ter vários bancos de dados.

### Database

Conjunto de Schemas. Os schemas e objetos de um banco de dados não podem ser compartilhados com outros. Um banco de dados compartilha com outros apenas usuários/roles e configurações do cluster.

Exemplos de comandos:

```sql
CREATE DATABASE name;
ALTER DATABASE name RENAME TO new_name;
DROP DATABASE name;
```

### Schema

Conjunto de objetos e relações. Vários schemas podem compor um banco de dados, tendo cada um seu conjunto de objetos e relações específicas. Pode ser visto, à grosso modo, como um agrupamento de objetos e relações de um subdomínio diferente da modelagem (ex: produtos, financeiro, clientes). 

É possível relacionar objetos entre diversos schemas.

Exemplos de comandos:

```sql
CREATE SCHEMA IF NOT EXISTS schema_name [AUTHORIZATION role_specification]; -- Authorization é uma formma de definir o dono (role) do shcme
ALTER SCHEMA schema_name RENAME TO new_schema_name;
DROP SCHEMA IF EXISTS schema_name;
```

### Objetos

São as tabelas, views, funções, types, sequences. É tudo que pode ser administrado dentro do schema. 

## Users/Roles/Groups

- Roles: papéis ou funções

- Users: usuários

- Groups: grupos de usuários (são "contas", perfis de atuação em um  banco de dados, que possuem permissões em comum ou específicas) 

Essas definições são mais formalidades, pois a partir do PostgreSQL 8.1 elas passaram a ser *alias*, uma das outras (todos tem o mesmo conceito). É como se tudo passasse a ser grupos (tendo suas permissões comuns e/ou específicas). É possível que roles/users/groups pertençam a outros roles/users/groups, criando assim, uma hierarquia de permissões.

Exemplos:

Imaginemos um banco de dados com as seguintes roles:

- Administrador (tem acesso irrestrito a todo o banco de dados)

- Professores (tem permissão de escrita nas tabelas "A" e "B", e leitura em todas as outras)
  
  - Professor Super (tem permissão de escrita na tabela "C" e herda todas as permissões de "Professor")
  
  - Professor Comum (Herda todas as permissões de "Professor")

- Alunos (tem permissão de leitura em todas as tabelas)
  
  - Aluno A (Herda todas as permissões de "Alunos")

Para criar role:

```sql
CREATE ROLE name [[WITH] options [...]];
```

Exemplos:

```sql
CREATE ROLE administradores
CREATEDB                      -- pode criar bancos de dados
CREATEROLE                    -- pode criar novas roles
INHERIT                       -- herda todas as permissoes da role a quem pertence
NOLOGIN                       -- não pode fazer login no banco de dados
REPLICATION                   -- pode fazer backup
BYPASSRLS                     -- isento das politicas de segurança RLS
CONNECTION LIMIT 1;           -- conexão ilimitada
```

```sql
CREATE ROLE professores
NOCREATEDB                    -- não pode criar bancos de dados
NOCREATEROLE                  -- não pode criar novas roles
INHERIT                       -- herda todas as permissoes da role a quem pertence
LOGIN                         -- pode fazer login no banco de dados
PASSWORD '123'                -- define senha de acesso
NOBYPASSRLS                   -- sujeito às politicas de segurança RLS
CONNECTION LIMIT 10;          -- máximo de conexões simultâneas
```

Para alterar role:

```sql
ALTER ROLE name [[WITH] options [...]];
```

 Para excluir uma role:

```sql
DROP ROLE name;
```

### Associação entre roles

Na criação da role:

```sql
CREATE ROLE professores_x
INHERIT                       -- herda todas as permissoes da role a quem pertence
IN ROLE professores           -- professores_x passa a pertencer à role professores
CONNECTION LIMIT 10;          
```

```sql
CREATE ROLE super_professores
INHERIT                       -- herda todas as permissoes da role a quem pertence
ROLE professores              -- professores passa a pertencer à role super_professores
CONNECTION LIMIT 10;          
```

Após a criação:

```sql
GRANT super_professores TO professores;
```

Assim, professores passa a pertencer a super_professores.

Para desfazer a associação:

```sql
REVOKE super_professores FROM professores;
```

fazendo com que professores não pertença mais a super_professores.

### Administrando acessos (GRANT/REVOKE)

Usamos GRANT/REVOKE privilégios para conceder e retirar privilégios de acesso ao banco de dados, respectivamente. Privilégios podem ser concedidos em vários níveis, como por exemplo,

de schema, de base de dados, de tabela, de coluna, entre outros.

#### Concedendo privilégios

Concedendo privilégios à nível de banco de dados:

```sql
GRANT {{CREATE | CONNECT | TEMPORARY | TEMP } [, ...] | ALL [PRIVILEGES]}
ON DATABASE database_name [, ....]
TO role_specification [, ...] [WITH GRANT OPTION]
```

Exemplo (concede permissão de criação na base escola para a role administradores):

```sql
GRANT CREATE PRIVILEGES ON DATABASE escola TO administradores;
```

Concedendo privilégios à nível de schema:

```sql
GRANT {{CREATE | USAGE } [, ...] | ALL [PRIVILEGES]}
ON SCHEMA schema_name [, ....]
TO role_specification [, ...] [WITH GRANT OPTION]
```

Exemplo (concede todos os privilégios no schema_x para a role administradores):

```sql
GRANT ALL PRIVILEGES ON schema_x TO administradores;
```

Concedendo privilégios à nível de tabela:

```sql
GRANT {{SELECT | INSERT | UPDATE | DELETE | TRUNCATE | REFERENCES | TRIGGER } [, ...] | ALL [PRIVILEGES]}
ON {TABLE table_name [, ....] | ALL TABLES IN SCHEMA schema_name [, ...] }
TO role_specification [, ...] [WITH GRANT OPTION]
```

Exemplo (concede permissão de seleção, criação, atualização e exclusão na tabela alunos para a role professores):

```sql
GRANT SELECT, INSERT, UPDATE, DELETE ON TABLE alunos TO professores;
```

#### Removendo privilégios

Tem sintaxe parecida com a concessão de privilégios.

Exemplo (removendo a permissão de exclusão da role professores na tabela alunos):

```sql
REVOKE DELETE ON TABLE alunos TO professores;
```

Removendo todas as permissões:

```sql
REVOKE ALL ON ALL TABLES IN SCHEMA schema_name FROM role; -- remove todas as permissoes em todas as tabelas do schema para a role
REVOKE ALL ON SCHEMA schema_name FROM role; -- remove todas as permissoes de schema para  a role
REVOKE ALL ON DATABASE database_name FROM role; -- remove todas as permissoes de banco de dados para a role
```

### # Common Table Expressions -  CTE

Forma auxiliar de organizar "statements", ou seja, blocos de códigos, para consultas muito grandes. gerando tabelas temporárias e criando relacionamentos entre elas.

Dentro dos statements podem ter SELECTs, INSERTs, UPDATEs ou DELETEs. 

A CTE é definida por meio dos WITH statements.

#### WITH Statements

Começando com o comando `WITH` define-se  o nome e a  lógica do statement - que fica dentro dos parêntesis. Cada statement pode ser entendido, à grosso modo, como uma fachada (façade),  pois isola o processamento complexo dele e disponibiliza um meio simples de acesso (a fachada). 

Cada WITH pode definir vários statements/fachadas/funções.

```sql
WITH [tbl_tmp_name_1] AS (
    SELECT [campos, ...]
    FROM table_a
    [JOIN]
    [WHERE]
), [tbl_tmp_name_2] AS (
    SELECT [campos, ...]
    FROM table_a
    [JOIN]
    [WHERE]
)
```

Após a criação dos stetements, temos as tabelas temporárias `tbl_tmp_name_1` e `tbl_tmp_name_2` sobre as quais podemos executar consultas. 

Exemplos:

```sql
SELECT campo_a FROM tbl_tmp_name_1
```

```sql
SELECT t1.campo_a, t2.campo_b
FROM tbl_tmp_name_1
JOIN tbl_tmp_name_2 t2 ON t2.campo_x = t1.campo_y;
```

Link para mais exemplos nas referências.



## Views



As views (visões) são "camadas" para as tabelas, são alias para uma ou mais queries.

Aceitam SELECT, INSERT, UPDATE, DELETE, sendo que se possuir JOINs aceitam apenas SELECT.



```sql
CREATE [ OR REPLACE ] [ TEMP | TEMPORARY ] [ RECURSIVE ] VIEW name [ (column_name [, ...]) ]
    [ WITH (view_option_name [= view_option_name] [,...]) ]
    AS query
    [WITH [ CASCADE | LOCAL ] CHECK OPTION]
```



- TEMP | TEMPORARY: view temporária só existe na seção do usuário, ao se desconectar a a view deixa de  existir.

- RECURSIVE: permite que a view seja chamada recursivamente

-  LOCAL CHECK OPTIONS: valida as opções da própria view
  
  - Exemplo: temos uma view para um tabela A, que filtra os resultados de um SELECT por um campo booleano com valor TRUE.
    
    - Ao tentar fazer um INSERT na tabela A pela view, passando o campo booleano como FALSE, ocorrerá um erro.

- CASCADED CHECK OPTIONS: valida as opções da view e das views que ela chama
  
  - Exemplo: temos uma view A para um tabela X, que filtra os resultados de um SELECT por um campo booleano com valor TRUE. Temos uma view B  para a view A  que filtra os seus resultados por um campo inteiro maior que 100.
    
    - Usando o cascaded na view B:
      
      - realizar um insert com o valor numerico menor que 100 gera erro (validação local)
      
      - realizar um insert com valor booleano false também gera erro (validação em cascata)



Exemplos:

Usa os nomes da tabela

```sql
CREATE OR REPLACE VIEW minha_view AS (
    SELECT id, nome
    FROM minha_tabela
);
```



## Savepoint transactions



Recurso que permite descartar parte das alterações dentro de uma transação retornando a um determinado estado anterior.



```sql
BEGIN;

    UPDATE conta 
        SET valor = valor  - 100 
    WHERE nome = 'Alice';

SAVEPOINT my_savepoint; -- cria um marco seguro até aqui

    UPDATE CONTA 
        SET valor = valor + 100 
    WHERE nome  = 'Bob'; -- ERRO simulado, o deposito era para Wally.

    UPDATE CONTA 
        SET valor 0; -- ops... zerou todas as contas

ROLLBACK TO my_savepoint; -- desfaz as alteracoes feitas depos de my_savepoint

    UPDATE CONTA 
        SET valor = valor + 100 
    WHERE nome  = 'Wally'; 

COMMIT;   
```





## Funções

Tem a finalidade de facilitar a programação e obter o reaproveitamento/reutilização de códigos.



Em Postgres existem quatro tipos de funções:

- Query language functions
  
  - Escritas em SQL

- [Procedural language functions](https://www.postgresql.org/docs/11/external-pl.html)
  
  - Escritas em outras linguagens de programação. 
    
    - Exemplo  PL/pgSQL (lingagem exclusiva do postgres)
    
    - Exemplo: PL/php

- Internal functions
  
  - Funções nativas
    
    - Exemplos: `substring`, `lpad`, dentro outras.

- C-language functions 
  
  - Funções escritas em C ou C++ que vem compiladas no postgres



Sintaxe:

```sql
CREATE [ OR REPLACE ] FUNCTION
   nome ( [ [ argmode ] [ argname ] argtype [ { DEFAULT | = } 
   default_expr ] [, ...] ] )
   [ RETURNS rettype
     | RETURNS TABLE ( column_nome column_type [, ...] ) ]
 { LANGUAGE lang_nome
   | WINDOW
   | IMMUTABLE | STABLE | VOLATILE | [ NOT ] LEAKPROOF
   | CALLED ON NULL INPUT | RETURNS NULL ON NULL INPUT | STRICT
   | [ EXTERNAL ] SECURITY INVOKER | [ EXTERNAL ] SECURITY DEFINER
   | COST execution_cost
   | ROWS result_rows
   | SET configuration_parameter { TO value | = value | FROM CURRENT }
   | AS 'definition'
   | AS 'obj_file', 'link_symbol'
 } ...
[ WITH ( attribute [, ...] ) ]
```



Detalhes de algumas opções: 



- REPLACE implica que a função mantenha:
  
  - o mesmo nome
  
  - o mesmo tipo de retorno
  
  - o mesmo número de parâmetro/argumentos

- RETURNS
  
  -  usando para informar o tipo de retorno

- LANGUAGE  
  
  - usado para definir a linguagem da função

- SECURITY
  
  - INVOKER (padrão)
    
    - permite que a função seja executada com as permissões do usuário que a executar
  
  - DEFINER
    
    - permite que a função seja executada com as permissões de quem criou a função

- IMMUTABLE (comportamento)
  
  - Não podem alterar o banco de dados.
  
  - Funções que garantem o mesmo resultado para os mesmos argumentos da função.
  
  - Evitar uso de SELECTs, pois as tabelas podem sofrer alterações.

- STABLE (comportamento)
  
  - Não podem alterar o banco de dados.
  
  - Funções que garantem o mesmo resultado para os mesmos argumentos da função.
  
  - Trabalha melhor com tipos de current_timestamps e outros variáveis.
  
  - Podem conter SELECTs

- VOLATILLE(comportamento) 
  
  - É o padrão do portgres
  
  - Aceita todos os cenários

- CALLED ON NULL INPUT (padrão)
  
  - Executa a função mesmo com argumentos não infomados ou null. 

- RETURNS NULL ON NULL INPUT
  
  - Retorna null se um dos parâmetros passados for null.

- COST
  
  - Calcula o custo/row em unidades de CPU.
  
  - Útil para otimização de consultas.

- ROWS
  
  - Número estimado de linhas que será analisada pelo planner (planejador de consultas).
  
  - Útil para otimização de consultas.



### SQL Functions

Não é possível usar transações.



Exemplos:

```sql
CREATE OR REPLACE FUNCTION fc_somar (INTEGER, INTEGER)
RETURNS INTEGER
LANGUAGE SQL
AS $$
    -- O código da função fica entre $$ $$
    SELECT $1 + $2;
$$;
```



```sql
CREATE OR REPLACE FUNCTION fc_somar (num1 INTEGER, num2 INTEGER)
RETURNS INTEGER
LANGUAGE SQL
AS $$
    SELECT num1 + num2; -- usando parametros nomeados
$$;
```



```sql
CREATE OR REPLACE FUNCTION fc_add_banco (p_numero INTEGER, p_nome VARCHAR, p_ativo BOOLEAN)
RETURNS TABLE (numero INTEGER,  nome VARCHAR) -- retorna linhas da tabela
LANGUAGE SQL
AS $$
    INSERT INTO bancos (numero, nome, ativo)
    VALUES (p_numero, p_nome, p_ativo);

    SELECT numero, nome FROM banco WHERE numero = p_numero; -- retorno
$$;
```



### PLPGSQL

Aceitam transações.



```sql
CREATE OR REPLACE FUNCTION fc_banco_exists (p_numero INTEGER)
RETURNS BOOLEAN    -- tipo de dado retornado
LANGUAGE PLPGSQL
AS $$
    DECLARE variavel_id INTEGER; -- delcara uma variavel
    BEGIN    -- início da trasação
            
        SELECT INTO variavel_id numero FROM banco WHERE p_numero = p_numero;
        IF variavel_id IS NULL THEN
           RETURN TRUE;     -- valor a ser retornado
        ELSE
            RETURN FALSE;    -- PGPSQL necessitar usar "RETURN"
        END IF;
     END;    -- fim da transação / commit
$$;
```





## Principais arquivos de configuração

### postgresql.conf

Arquivo onde estão definidas e armazenadas todas as configurações do servidor PostgreSQL (exemplos: configuração de memória, número de usuários, registro de erros, replicação) .
Normalmente fica localizado dentro do diretório de dados (PGDATA)

A view pg_settings, acessada por dentro do banco de dados, exibe todas as configurações atuais. Algumas configurações só entram em funcionamento após reiniciar o banco de dados.

Para ver todas as configurações atuais:

```sql
SELECT name, settings FROM pg_settings;
```

É possível também visualizar uma configuração específica executando:

```sql
SHOW [parametro]
```

#### Configurações de conexão

- LISTEN_ADDRESS
  - Endereço(s) TCP/IP das interfaces que servidor PostgreSQL vai escutar/liberar conexões.
- PORT
  - A porta TCP que o servidor PostgreSQL vai ouvir (5432, é a porta padrão)
- MAX_CONNECTIONS
  - Número máximo de conexões suportadas pelo servidor PostgreSQL.
- SUPERUSER_RESERVED_CONNECTIONS
  - Número de conexões (slots) reservadas (do máximo conexões suportadas) para conexões exclusivas de super usuários ao banco de dados.

#### Configurações de autenticação

- AUTHENTICATION_TIMEOUT
  - Tempo máximo, em segundos, para o cliente conseguir uma conexão com o servidor.
- PASSWORD_ENCRYPTATION
  - Algoritmo de criptografia das senhas dos novos usuários criados no banco de dados.
- SSL
  - Habilita a conexão criptografada por SSL
    - Somente se o PostgreSQL foi compilado com suporte SSL

#### Configurações de memória

- SHARED_BUFFERS
  - Tamaho da memória compartilhada do servidor PostgreSQL para cache/buffer de tabelas, índices e demais relações
- WORK_MEM
  - Tamanho da memória (exclusiva) para operações de agrupamento e ordenação (ORDER BY, DISTINCT, MERGE, JOINS).
- MAINTENANCE_WORK_MEM
  - Tamanho da memória (exclusiva) para operações com VACUUM, INDEX, ALTER TABLE.

## pg_hba.conf

Arquivo responsável pelo controle de autenticação dos usuários no servidor PostgreSQL. Define os endereços de onde os usuários podem acessar o banco.

Observação: por padrão, só é permitido o acesso, via linha de comando, com o usuário "postgres". Logo para acessar, alterne para o usuário `su - postegres` e execute `psql`.

Alguns métodos de autenticação:

- TRUST (conexão sem requisição de senha)

- REJECT (rejeitar conexões)

- MD5 (criptografia MD5)

- PASSWORD (senha sem criptografia)

- IDENT (utiliza o usuário do SO do cliente via ident server)

- PEER (utiliza o usuário do SO do cliente)

- LDAP (ldap server)

- CERT (via ceriticado ssl do cliente)

Exemplo 

```
# TYPE                DATABASE                ADDRESS                METHOD
 local                all                                            peer 
 host                 all                     127.0.0.1/32           md5

## Abaixo configuração de qual usuário e endereço pode fazer backup
local                 replication                                    peer       
host                  replication             ::1/128                md5 
```

## pg_ident.conf

Arquivo responsável por mapear os usuários do sistema operacional com os usuários do banco de dados. Fica localizado do diretório de dados PGDATA. 

Exemplo 

```
# MAPNAME                SYSTEM-USERNAME                    PG-USERNAME
dev                      andre                              pg_dev
dev                      erdna                              pg_dev
dba                      luiz                               pg_dba
```

## Referências

[Digital Innovation One](https://www.youtube.com/channel/UCMxqhdELkftE8DuBZiwdPfg/search?query=postgres)

[O que é o PostgreSQL?](http://pgdocptbr.sourceforge.net/pg82/intro-whatis.html)

[PostgreSQL + Docker: executando uma instância e o pgAdmin 4 a partir de containers](https://medium.com/@renato.groffe/postgresql-docker-executando-uma-instância-e-o-pgadmin-4-a-partir-de-containers-ad783e85b1a4)

[PostgreSQL + pgAdmin 4 + Docker Compose: montando rapidamente um ambiente para uso](https://medium.com/@renato.groffe/postgresql-pgadmin-4-docker-compose-montando-rapidamente-um-ambiente-para-uso-55a2ab230b89)

[Simplificando o SQL — #Parte 1 — CTE — common table expressions (WITH Queries) [PostgreSQL]](https://medium.com/@natanmedeirosrn/simplificando-o-sql-parte-1-cte-common-table-expressions-with-queries-postgresql-43eed6707110)
