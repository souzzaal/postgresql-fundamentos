SELECT numero, nome, ativo
FROM banco;

-- usa os mesmos nomes da tabela banco
CREATE OR REPLACE VIEW vw_bancos AS (
	SELECT numero, nome, ativo
	FROM banco
);
SELECT * FROM vw_bancos;

-- cria alias para os nomes da tabela banco
CREATE OR REPLACE VIEW vw_bancos2 (banco_numero, banco_nome, banco_ativo) AS (
	SELECT numero, nome, ativo
	FROM banco
);
SELECT banco_numero FROM vw_bancos2;

INSERT INTO vw_bancos2 VALUES (51, 'Banco boa ideia', TRUE);
SELECT * FROM vw_bancos2 WHERE banco_numero = 51;

UPDATE vw_bancos2 SET banco_ativo = FALSE WHERE banco_numero = 51;
DELETE FROM vw_bancos2 WHERE banco_numero = 51;


-- cria view temporária
CREATE OR REPLACE TEMPORARY VIEW vw_agencias AS (
	SELECT nome
	FROM agencia
);
SELECT nome FROM vw_agencias;

--  WITH LOCAL CHECK OPTION
CREATE OR REPLACE VIEW vw_bancos_ativos AS (
	SELECT numero, nome, ativo
	FROM banco
	WHERE ativo IS TRUE
) WITH LOCAL CHECK OPTION;
INSERT INTO vw_bancos_ativos VALUES (51, 'Banco boa ideia', FALSE); -- erro 

CREATE OR REPLACE VIEW vw_bancos_com_a AS (
	SELECT numero, nome, ativo
	FROM vw_bancos_ativos
	WHERE nome ILIKE 'a%'
) WITH LOCAL CHECK OPTION;
SELECT * FROM vw_bancos_com_a;

INSERT INTO vw_bancos_com_a VALUES (51, 'Banco A', TRUE); -- erro validacao local em vw_bancos_com_a
INSERT INTO vw_bancos_com_a VALUES (51, 'A Banco', FALSE); -- erro validacao local em vw_bancos_ativos
DELETE FROM vw_bancos2 WHERE banco_numero = 51;

--  WITH LOCAL CHECK OPTION
CREATE OR REPLACE VIEW vw_bancos_ativos AS (
	SELECT numero, nome, ativo
	FROM banco
	WHERE ativo IS TRUE
); -- sem validacao local
INSERT INTO vw_bancos_ativos VALUES (51, 'Banco boa ideia', FALSE); -- nao gera erro de valicao do campo ativo
DELETE FROM vw_bancos2 WHERE banco_numero = 51;

CREATE OR REPLACE VIEW vw_bancos_com_a AS (
	SELECT numero, nome, ativo
	FROM vw_bancos_ativos
	WHERE nome ILIKE 'a%'
) WITH CASCADED CHECK OPTION;
SELECT * FROM vw_bancos_com_a;

INSERT INTO vw_bancos_com_a VALUES (51, 'Banco A', TRUE); -- erro validacao local em vw_bancos_com_a
INSERT INTO vw_bancos_com_a VALUES (51, 'A Banco', FALSE); -- erro validacao cascata em vw_bancos_ativos


-- VIEW RECURSIVA
CREATE TABLE IF NOT EXISTS funcionarios (
	id SERIAL PRIMARY KEY,
	nome VARCHAR(50),
	gerente INTEGER,
	FOREIGN KEY (gerente) REFERENCES funcionarios(id)
);

INSERT INTO funcionarios 
	(nome, gerente)
VALUES
	('Ancelmo', null),
	('Beatriz', 1),
	('Magno', 1),
	('Cremilda', 2),
	('Vagner', 4),
	('Agelo', 1),
	('Maria', 4),
	('Joao', 2),
	('Fulano', 2),
	('Sicrano', 1),
	('Beltrano', 4);
	
SELECT * FROM funcionarios WHERE gerente IS NULL;

-- tem o mesmo resultado que o select acima pois não exitem resultados para id = 999
SELECT * FROM funcionarios WHERE gerente IS NULL
UNION ALL
SELECT * FROM funcionarios WHERE id = 999;

-- diferenca entre UNION e UNION ALL
SELECT 1 UNION SELECT 1; -- agrupa resultados iguais
SELECT 1 UNION ALL SELECT 1; -- não agrupa resultados iguais

CREATE OR REPLACE RECURSIVE VIEW vw_func(id, gerente, funcionario) AS (
	SELECT * FROM funcionarios WHERE gerente IS NULL 						-- parte a
	UNION ALL
	SELECT f.* FROM funcionarios f JOIN vw_func ON vw_func.id = f.gerente	-- parte b
);

-- A "parte a" retorna apenas "Anselmo".
-- A "parte b" chama vw_func, gerando o loop recursivo até zerarem as opçoes, isto é, 
-- exibindo o gerente e logo a baixo seus subordinados.

SELECT id, gerente, funcionario FROM vw_func;


